exports.userdetails = function(req,res){
	if(req.session.username != null && req.session.username != ""){
		global.accountoperation.renderUserDetailsPage({},res,req);
	}
	else
		global.accountoperation.userUnverified(res, "Invalid Session Please Login to Continue!!", {}, req);
};

exports.getLifeEvents = function(req,res){
	if(req.session.username != null && req.session.username != ""){
		global.accountoperation.getLifeEvents({},res,req);
	}
	else
		global.accountoperation.userUnverified(res, "Invalid Session Please Login to Continue!!", {}, req);
};

exports.uploadProfilePic = function(req,res){
	if(req.session.username != null && req.session.username != ""){
		console.log(req);
		var fs = require('fs');
		fs.readFile(req.files.pofilepic.path, function (err, data) {
			fs.exists(req.files.pofilepic.path)
			  var newPath = "/home/rakshithk/workspace/SocialMediaApplicationPrototypeRabbitMQandMongoDB/public/uploads/"+req.files.pofilepic.name;
			  console.log("File newPath " + "");
			  fs.writeFile(newPath, data, function (err) {
				  console.log("File Uploaded" + err);
				  global.accountoperation.updateProfilePicture({newPath: "/public/uploads/"+req.files.pofilepic.name},res,req);
		  });
		});
	}
	else
		global.accountoperation.userUnverified(res, "Invalid Session Please Login to Continue!!", {}, req);
};
