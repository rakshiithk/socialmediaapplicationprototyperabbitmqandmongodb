global.DBSource = "MQMongoDB";//Name of the Datasource to the accessed

if(global.DBSource == "MQMongoDB"){
	global.accountoperation = require('./accountoperationMQMongo');
}
else{
	global.accountoperation = require('./accountoperation');//Default DBSource is "MySQL"
}
global.accountoperation.setup();

exports.index = function(req, res){
	if(req.session.username != "" && req.session.username != null){
		global.accountoperation.verifyUser(req.body,res,req);
	}
	else
		res.render('index', { user: JSON.stringify({}),login:JSON.stringify({})});
};