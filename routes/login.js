var requestId = 0;

exports.loginfail = function(req,res){
	var newuser = {};
	newuser.errorMessage = "Invalid Login Credentials!! Please ensure your Username/Password is correct...";
	newuser.errorInloginForm = true;
	res.render('index', { user: JSON.stringify({}),login:JSON.stringify(newuser)});
};

exports.login = function(req,res){
	var user = req.body;
	console.log("Login Invoked \n" + JSON.stringify(user));
	if(req.session != null && req.session != ""){
		console.log(req.isAuthenticated() + "req.isAuthenticated()");
		if(req.session.passport != null && req.session.passport != ""){
			req.session.username = req.session.passport.user.EMAIL_ADDR;
			req.session.firstname = req.session.passport.user.FIRST_NAME;
			req.session.lastname = req.session.passport.user.LAST_NAME;
			req.session.ROW_ID = req.session.passport.user.ROW_ID;
			req.session.IMAGE_URL = req.session.passport.user.IMAGE_URL;
			delete req.session.passport;
		}
	}
	global.accountoperation.verifyUser(user,res,req);
	console.log("req.sessions passport + " + JSON.stringify(req.session));
};

exports.loginlocal = function(req,res){
	var user = req.body;
	console.log("loginlocal Invoked \n");
	global.accountoperation.loginlocal(user,res,req);
};

exports.home = function(req,res){
	var user = req.body;
	console.log("Home Redirection Invoked");
	console.log(req.session);
	console.log("login Usser");
	if((req.session.username != null && req.session.username != "") || req.session != {})
		global.accountoperation.verifyUser(user,res,req);//DBConnection.handleDBRequest("loginUser",user,res,req);
	else
		global.accountoperation.userUnverified(res, "Invalid Session Please Login to Continue!!", user, req);
};

exports.logout = function(req,res){
	var user = req.body;
	req.session.destroy();
	console.log(JSON.stringify(req.session));
	res.status(200).send("Session Terminated!! Please close the browser for Safety.");
	//res.redirect('/');
};