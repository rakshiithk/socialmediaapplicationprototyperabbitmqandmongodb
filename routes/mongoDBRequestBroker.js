var mongo = require("../routes/mongo");
var mongoURL = "mongodb://localhost:27017/SocialMediaPrototypeDB";
var async = require('async');
var requestId = 0;

function deligateDBAccessRequest(operation,data,res,req,requestId,rpc){

	console.log("handleDBRequest - " + operation +" with requestId : " + requestId);

	switch(operation){
	case "createUser" :
		console.log("CreateUser for Mongo");
		var bcrypt = require('bcrypt');

		//Getting User Object from Request
		var FIRST_NAME = data.firstname;
		var LAST_NAME = data.lastname;
		var EMAIL_ADDR = data.emailM;
		var DATE_OF_BIRTH = data.dateofbirth;
		var PASSWORD = data.password;
		var GENDER = data.gender;

		if(FIRST_NAME != null && FIRST_NAME != "" 
			&& LAST_NAME != null && LAST_NAME != ""
				&& EMAIL_ADDR != null && EMAIL_ADDR != ""
					&& DATE_OF_BIRTH != null && DATE_OF_BIRTH != ""
						&& PASSWORD != null && PASSWORD != ""
							&& GENDER != null && GENDER != ""){

			//Getting Salt Value and Encrypting Password
			var salt = bcrypt.genSaltSync(10);
			data.password = bcrypt.hashSync(PASSWORD, salt);	

			//Debug information
			console.log(FIRST_NAME);
			console.log(LAST_NAME);
			console.log(EMAIL_ADDR);
			console.log(DATE_OF_BIRTH);
			console.log(PASSWORD);
			console.log(GENDER);
			console.log(PASSWORD);
			console.log(data.IMAGE_URL);

			if(data.IMAGE_URL == null || data.IMAGE_URL == "")
				data.IMAGE_URL = "/images/profile-photo_default.jpg";

			var fieldmap = { firstname: "FIRST_NAME" , lastname: "LAST_NAME" , emailM: "EMAIL_ADDR" , dateofbirth: "DATE_OF_BIRTH" , password: "PASSWORD" , gender: "GENDER",IMAGE_URL : "IMAGE_URL"};

			var newuser = {};

			for (var key in data) {
				if (data.hasOwnProperty(key) && fieldmap[key] != null && fieldmap[key] != "") {
					console.log(key + " -> " + data[key]);
					newuser[fieldmap[key]] = data[key]
				}
			}
			console.log("newuser to be created : " + newuser);
			executeInsertQuery(null,newuser,req,res,operation,requestId,rpc);
		}
		else{
			var accountoperation = global.accountoperation;
			accountoperation.userCreationError(res, "There was an Error Creating the user due to missing information!! Please try Again later.", data, req);
		}
		break;
		
	case "loginlocal" :
		console.log("loginlocal : " + operation);	
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
		}
	
		if(data.username != null && data.username != ""){
			if( data.username == null ||  data.username == "")
				data.username = req.session.username;
	

			mongo.connect(mongoURL, function(){ 
				console.log('Connected to mongo at: ' + mongoURL); 
				var coll = mongo.collection('USERS'); 
				coll.findOne({EMAIL_ADDR: data.username},function(err,user){
					if(!err){
						if(user != null){
							var bcrypt = require('bcrypt');
							if (bcrypt.compareSync(data.password,user.PASSWORD)) {
								res.status(200).send("Sucessfull Login");
							}else{
								res.status(401).send("Invalid Credentials!!Please try again..");
							}
						}else{
							//User Not Found
							res.status(401).send("Invalid Credentials!!Please try again..");
						}
					}else{
						//Unknown Error
						res.status(401).send("Error Loging into the Server!!Please try again..");
					}
				});
			});
		}
		else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Your Session is no Longer Valid!! Please login again to Proceed.",{},req);
		}
		break;

	case "verifyUser" :
	case "loginUser" :
		console.log("loginUser : " + operation);	
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
		}

		if(data.username != null && data.username != ""){
			if( data.username == null ||  data.username == "")
				data.username = req.session.username;

			executeSelectQuery(null,data,req,res,operation,requestId,rpc);	
		}
		else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Your Session is no Longer Valid!! Please login again to Proceed.",{},req);
		}
		break;

	case "getNewsFeed" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}
		break;

	case "postStatusUpdate" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;

			var newpost = {};

			newpost.USER_ID = req.session.ROW_ID;
			newpost.POST_MESSAGE = req.body.POST_MESSAGE;
			newpost.FIRST_NAME = req.session.firstname;
			newpost.LAST_NAME = req.session.lastname;
			newpost.IMAGE_URL = req.session.IMAGE_URL; 

			console.log("postStatusUpdate newpost : " + newpost);
			executeInsertQuery(null,newpost,req,res,operation,requestId,rpc);

		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "sendFiendRequest" :
		if(req.session.username != null && req.session.username != ""){
			var newfriendReq = {};
			newfriendReq.username =  req.session.username;
			newfriendReq.firstname = req.session.firstname;
			newfriendReq.lastname = req.session.lastname;
			newfriendReq.ROW_ID = req.session.ROW_ID;
			newfriendReq.IMAGE_URL = req.session.IMAGE_URL;
			newfriendReq.USER1 = req.session.ROW_ID;
			newfriendReq.friend = req.body.friend;
			executeInsertQuery(null,newfriendReq,req,res,operation,requestId,rpc);

		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderFriendListPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadMyFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadPendingFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "rejectFriendRequest" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			data.friend = req.body;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req);
		}
		break;

	case "acceptFriendRequest" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			data.friend = req.body;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderUserDetailsPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getLifeEvents" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "updateProfilePicture" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			req.session.IMAGE_URL = data.newPath;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderGroupsPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadAllGroups" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadMyGroups" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "addUserToGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.USER_ID = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "removeUserFromGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.USER_ID = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "createGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.CREATED_BY = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "unFriendUserRequest" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.ROW_ID = req.session.ROW_ID;
			data.friendid = req.body.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "navToGroupDetailPage" :
		if(req.session.username != null && req.session.username != ""){
			data.ROW_ID = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupDetails" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = req.session.groupid;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupUserList" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = req.session.groupid;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupNonMembers" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = req.session.groupid;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;


	case "addUserToGroupAdmin" :
		if(req.session.username != null && req.session.username != ""){
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "removeUserFromGroupAdmin" :
		if(req.session.username != null && req.session.username != ""){
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "deleteGroup" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.GROUP_ID = req.session.groupid
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "navToFriendDetailPage" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.ROW_ID = req.session.ROW_ID;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getFriendDetails" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.friendid = req.session.friendid;
			executeSelectQuery(null,data,req,res,operation,requestId,rpc);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;


	default:
		break;
	}
}

function executeSelectQuery(sql_stmt,data,req,res,operation,requestId,rpc){
	try {
		console.log("executeSelectQuery for " + operation + " : Sending Data : " + JSON.stringify(data));
		rpc.makeRequest(operation, data, function(error, response) {
			console.log("Response from Mongo for " + operation + "  : "  + JSON.stringify(response));
			if (!response || response.code == 200) {
				if (operation == "verifyUser"
					|| operation == "loginUser" 
						|| operation == "renderFriendListPage"
							|| operation == "renderUserDetailsPage" 
								|| operation == "updateProfilePicture" 
									|| operation == "renderGroupsPage"
										|| operation == "navToGroupDetailPage"
											|| operation == "deleteGroup"
												|| operation == "navToFriendDetailPage") {
					try {
						console.log("exisiting data : " + JSON.stringify(data));
						if (data == null || data == "")
							data = {};
						if (req.session.username != ""	&& req.session.username != null) {
							console.log("Session found User Detials being returned !!");
							var fieldmap = {
									ROW_ID : "ROW_ID",
									FIRST_NAME : "firstname",
									LAST_NAME : "lastname",
									EMAIL_ADDR : "emailM",
									DATE_OF_BIRTH : "dateofbirth",
									GENDER : "gender",
									IMAGE_URL : "IMAGE_URL"
							};

							for ( var key in response) {
								if (response.hasOwnProperty(key) && fieldmap[key] != null && fieldmap[key] != "" && response[key] != ""	&& response[key] != null) {
									data[fieldmap[key]] = response[key];
								}
							}
							console.log("data  Final before operations: " + data);
							if (operation == "verifyUser")
								accountoperation.userVerified(data, res, req);
							else if (operation == "loginUser") {
								console.log("Test" + operation);
								accountoperation.homeRedirection(data, res, req);
							}else if(operation == "renderFriendListPage"){
								console.log("Test" + operation);
								accountoperation.FriendListPageRedirect(data,res, req);
							}else if(operation == "renderUserDetailsPage"){
								console.log("Test" + operation);
								accountoperation.userDetailsPageRedirect(response,res, req);
							}else if(operation == "updateProfilePicture"){
								accountoperation.renderUserDetailsPage({},	res, req);
							}else if(operation == "renderGroupsPage"){
								console.log("Test" + operation);
								accountoperation.groupPageRedirect(response,res, req);
							}else if(operation == "navToGroupDetailPage"){
								console.log("Test" + operation);
								accountoperation.groupDetailsPageRedirect(response,res, req);
							}else if (operation == "deleteGroup") {
								console.log("Test" + operation);
								accountoperation.userVerified(data,	res, req);
							}else if(operation == "navToFriendDetailPage"){
								console.log("Test" + operation);
								accountoperation.friendDetailsPageRedirect(response,res, req);	
							}
						} else {
							console.log("no session found!!");
							var bcrypt = require('bcrypt');
							if (bcrypt.compareSync(data.password,response.PASSWORD)) {

								var fieldmap = {_id : "ROW_ID",FIRST_NAME : "firstname",LAST_NAME : "lastname",EMAIL_ADDR : "emailM",DATE_OF_BIRTH : "dateofbirth",GENDER : "gender",IMAGE_URL : "IMAGE_URL"};

								for ( var key in response) {
									if (response.hasOwnProperty(key) && fieldmap[key] != null	&& fieldmap[key] != ""	&& response[key] != "" && response[key] != null) {
										console.log(key	+ " No Session -> "	+ response[key]);
										data[fieldmap[key]] = response[key];
									}
								}
								accountoperation.userVerified(data, res, req);
								console.log("User Verified !!! ");
							} else {
								console.log("User Not Verfied !!! ");
								data.password = "";
								accountoperation.userUnverified(res,"Incorrect username or Password Specified",	data, req);
							}
						}

					} catch (e) {
						console.log(e.toString());
						console.log("User Not Verfied !!! ");
						data.password = "";
						accountoperation.userUnverified(res,"Incorrect username or Password Specified",	data, req);
					}
				}
				//Async Request which dont needs rendering of pages
				if (operation == "getNewsFeed"
					|| operation == "loadFriendList"
						|| operation == "loadMyFriendList"
							|| operation == "loadPendingFriendList"
								|| operation == "rejectFriendRequest"
									|| operation == "acceptFriendRequest" 
										|| operation == "getLifeEvents"
											|| operation == "loadAllGroups"
												|| operation == "loadMyGroups"
													||operation == "addUserToGroup"
														|| operation == "removeUserFromGroup"
															|| operation == "createGroup"
																|| operation == "unFriendUserRequest"
																	|| operation == "getGroupDetails"
																		|| operation == "getGroupUserList"
																			|| operation == "getGroupNonMembers"
																				|| operation == "addUserToGroupAdmin"
																					|| operation == "removeUserFromGroupAdmin"
																						|| operation == "getFriendDetails") {
					try{
						if(response)
							res.status(200).send(JSON.stringify(response.content));
						else
							res.status(401).send();
					}catch(e){
						console.log(e);
						res.status(401).send();
					}

				}
			}else{
				if (operation == "verifyUser" 
					|| operation == "loginUser"
						||  operation == "renderFriendListPage"
							|| operation == "renderUserDetailsPage"
								|| operation == "updateProfilePicture"
									|| operation == "renderGroupsPage"
										|| operation == "navToGroupDetailPage"
											|| operation == "deleteGroup"
												|| operation == "navToFriendDetailPage") {
					var newuser = {};
					newuser.username = data.username;
					console.log(error);
					console.log(newuser);
					accountoperation.userUnverified(res,"Incorrect username or Password Specified",newuser, req);
				}
				if (operation == "getNewsFeed"
					|| operation == "loadFriendList"  
						|| operation == "loadMyFriendList"
							|| operation == "loadPendingFriendList" 
								|| operation == "rejectFriendRequest"
									|| operation ==  "acceptFriendRequest"
										|| operation == "getLifeEvents"
											||operation == "loadAllGroups"
												|| operation == "loadMyGroups"
													||operation == "addUserToGroup"
														|| operation == "removeUserFromGroup"
															|| operation == "createGroup"
																|| operation == "unFriendUserRequest"
																	|| operation == "getGroupDetails"
																		|| operation == "getGroupUserList"
																			|| operation == "getGroupNonMembers"
																				|| operation == "addUserToGroupAdmin"
																					|| operation == "removeUserFromGroupAdmin"
																						|| operation == "getFriendDetails") {
					res.status(403).send(error);
				}

			}
		});
	} catch (e) {
		console.log(e);
	}finally{}
}


function executeInsertQuery(sql_stmt,data,req,res,operation,requestId,rpc){
	try {
		console.log("Got Connection and Executing Query !! (requestId) ==> "+ requestId +" ==>> " + sql_stmt);
		rpc.makeRequest(operation, data, function(err, response) {
			console.log("Response from Mongo " + JSON.stringify(response));
			if (!response || response.code == 200) {
				if (operation === "createUser") {
					var fieldmap = {
							FIRST_NAME : "firstname",
							LAST_NAME : "lastname",
							EMAIL_ADDR : "emailM",
							DATE_OF_BIRTH : "dateofbirth",
							PASSWORD : "password",
							GENDER : "gender",
							IMAGE_URL : "IMAGE_URL"
					};

					var newuser = {};

					for ( var key in data) {
						if (data.hasOwnProperty(key) && fieldmap[key] != null
								&& fieldmap[key] != "" && data[key] != ""
									&& data[key] != null) {
							console.log(key + " -> " + data[key]);
							newuser[fieldmap[key]] = data[key]
						}
					}
					newuser.ROW_ID = response.ROW_ID;
					console.log("Final newuser : " + JSON.stringify(newuser));
					accountoperation.userCreated(res, newuser, req);
				}
				if (operation == "postStatusUpdate" || operation == "sendFiendRequest") {
					res.status(200).send(response);
				}
			} else {
				if (operation === "createUser") {
					var fieldmap = {
							FIRST_NAME : "firstname",
							LAST_NAME : "lastname",
							EMAIL_ADDR : "emailM",
							DATE_OF_BIRTH : "dateofbirth",
							PASSWORD : "password",
							GENDER : "gender"
					};

					var newuser = {};

					for ( var key in data) {
						if (data.hasOwnProperty(key) && fieldmap[key] != null
								&& fieldmap[key] != "" && data[key] != ""
									&& data[key] != null) {
							console.log(key + " -> " + data[key]);
							newuser[fieldmap[key]] = data[key]
						}
					}
					console.log(response.err);
					console.log(newuser);
					accountoperation.userCreationError(res, response.err, newuser, req);
				}
				if (operation == "postStatusUpdate" || operation == "sendFiendRequest") {
					res.status(403).send(err);
				}
			}
		});
	} catch (e) {
		console.log(e);
	}
	finally{	
	}
}

module.exports = {
	handleDBRequest : function(operation,data,res,req,rpc){
		console.log("handleDBRequest got the Request to '" + operation +"' and its handled with RequestId : " + requestId);
		deligateDBAccessRequest(operation,data,res,req,requestId,rpc);
		requestId++;
	}
};
