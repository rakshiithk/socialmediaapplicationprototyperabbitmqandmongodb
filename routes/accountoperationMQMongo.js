var amqp = require('amqp');
var connection = amqp.createConnection({host:'127.0.0.1'});
var rpc = new (require('../rpc/amqprpc'))(connection);

module.exports = {
		setup : function(){
			console.log("Global : " + global.DBSource );
			console.log("RabbitMQ Connection Setup!!");
		},
		createUser : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("createUser",user,res,req,rpc);
		},
		loginlocal : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loginlocal",user,res,req,rpc);
		},
		userCreationError : function(res,error,newuser,req){
			console.log("userCreationError == " + error);
			newuser.errorMessage = error;
			newuser.emailR = newuser.emailM;
			newuser.password = "";
			newuser.errorInForm = true;
			res.render('index', { user: JSON.stringify(newuser),login:JSON.stringify({})});
		},
		userCreated : function(res,data,req){
			data.username = data.emailR;
			userLogin(res,data,req);
		},
		verifyUser : function(user,res,req){
			console.log("Mongo Connection");
			var DBConnection = require('./mongoDBRequestBroker');
			if(req.session.username != null && req.session.username != ""){
				var user = req.body;
				DBConnection.handleDBRequest("loginUser",user,res,req,rpc);
			}else{
				console.log("Verify User in Account Operation\n ");
				DBConnection.handleDBRequest("loginUser",user,res,req,rpc);	
			}
		},
		userVerified : function(user,res,req){
			userLogin(res,user,req);
		},
		userUnverified : function(res,error,user,req){
			if(error != null && error != ""){
				user.errorInloginForm = true;
				user.errorMessage = error;	
			}
			res.render('index', { user: JSON.stringify({}),login:JSON.stringify(user)});
		},
		homeRedirection : function(user,res,req){
			console.log("Finally Responsed " + JSON.stringify(user));
			res.render('home', { user: JSON.stringify(user)});	
		},
		getNewsFeeds : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getNewsFeed",user,res,req,rpc);
		},
		postStatusUpdate : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("postStatusUpdate",user,res,req,rpc);
		},
		loadFriendList : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loadFriendList",user,res,req,rpc);
		},
		sendFiendRequest : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("sendFiendRequest",user,res,req,rpc);
		},
		renderFriendListPage : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("renderFriendListPage",user,res,req,rpc);
		},
		FriendListPageRedirect : function(data,res, req){
			console.log(data);
			res.render('friendslist', { user: JSON.stringify(data)});	
		},
		loadMyFriendList : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loadMyFriendList",user,res,req,rpc);
		},
		loadPendingFriendList : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loadPendingFriendList",user,res,req,rpc);
		},
		rejectFriendRequest : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("rejectFriendRequest",user,res,req,rpc);
		},
		acceptFriendRequest : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("acceptFriendRequest",user,res,req,rpc);
		},
		renderUserDetailsPage : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("renderUserDetailsPage",user,res,req,rpc);
		},
		userDetailsPageRedirect : function(data,res, req){
			console.log(data);
			if(global.DBSource == "MQMongoDB")
				res.render('userdetails', { user: JSON.stringify(data.content)});
			else
				res.render('userdetails', { user: JSON.stringify(data)});	
		},
		getLifeEvents : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getLifeEvents",user,res,req,rpc);
		},
		updateProfilePicture : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("updateProfilePicture",user,res,req,rpc);
		},
		renderGroupsPage  : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("renderGroupsPage",user,res,req,rpc);
		},
		groupPageRedirect : function(data,res, req){
			console.log(data);
			if(global.DBSource == "MQMongoDB")
				res.render('groups', { user: JSON.stringify(data.content)});
			else
				res.render('groups', { user: JSON.stringify(data)});	
		},
		loadAllGroups : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loadAllGroups",user,res,req,rpc);
		},
		loadMyGroups : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("loadMyGroups",user,res,req,rpc);
		},
		addUserToGroup : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("addUserToGroup",user,res,req,rpc);
		},
		removeUserFromGroup : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("removeUserFromGroup",user,res,req,rpc);
		},
		createGroup : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("createGroup",user,res,req,rpc);
		},
		unFriendUserRequest : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("unFriendUserRequest",user,res,req,rpc);
		},
		navToGroupDetailPage : function(user,res,req){
			user.groupid = req.query.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("navToGroupDetailPage",user,res,req,rpc);
		},
		groupDetailsPageRedirect : function(data,res, req){
			console.log(data);
			if(global.DBSource == "MQMongoDB")
				res.render('groupdetails', { user: JSON.stringify(data.content), groupid : req.query.groupid });
			else
				res.render('groupdetails', { user: JSON.stringify(data), groupid : req.query.groupid });	
		},
		getGroupDetails : function(user,res,req){
			user.groupid = req.session.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getGroupDetails",user,res,req,rpc);
		},
		getGroupUserList : function(user,res,req){
			user.groupid = req.session.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getGroupUserList",user,res,req,rpc);
		},
		getGroupNonMembers : function(user,res,req){
			user.groupid = req.session.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getGroupNonMembers",user,res,req,rpc);
		},
		removeUserFromGroupAdmin : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("removeUserFromGroupAdmin",user,res,req,rpc);
		},
		addUserToGroupAdmin : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("addUserToGroupAdmin",user,res,req,rpc);
		},
		deleteGroup  : function(user,res,req){
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("deleteGroup",user,res,req,rpc);
		},
		navToFriendDetailPage : function(user,res,req){
			user.groupid = req.query.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("navToFriendDetailPage",user,res,req,rpc);
		},
		friendDetailsPageRedirect : function(data,res, req){
			console.log(data);
			if(global.DBSource == "MQMongoDB")
				res.render('frienddetails', { user: JSON.stringify(data.content), friendid : req.query.friendid });
			else
				res.render('frienddetails', { user: JSON.stringify(data), friendid : req.query.friendid });	
		},
		getFriendDetails : function(user,res,req){
			user.groupid = req.session.groupid;
			var DBConnection = require('./mongoDBRequestBroker');
			DBConnection.handleDBRequest("getFriendDetails",user,res,req,rpc);
		}
};

function userLogin(res,data,req){
	var userName = data.username;
	console.log("userName " + userName);
	if(req.session.username == "" || req.session.username == null){
		req.session.username = data.emailM;
		req.session.firstname = data.firstname;
		req.session.lastname = data.lastname;
		req.session.ROW_ID = data.ROW_ID;
		req.session.IMAGE_URL = data.IMAGE_URL;
		console.log("Session set" + req.session.username + "," + req.session.ROW_ID + "," + req.session.firstname + ", " + req.session.IMAGE_URL);
	}else
		console.log("Exisiting Session " + req.session.username + "," + req.session.ROW_ID + "," + req.session.firstname + ", " + req.session.IMAGE_URL);
	res.redirect("/home");//Default Action !!
	//res.status(200).send("Sucessful Login");
};